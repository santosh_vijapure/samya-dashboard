import Text from 'antd/lib/typography/Text'
import React, { useEffect } from 'react'
import { ArrowUpOutlined,ArrowRightOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import { Chart } from '@antv/g2';



const data1 = [];
for (let i = 0; i < 100; i++) {
  data1.push({
    type: i + '',
    value: 10,
  });
}

const data2 = [];
for (let i = 0; i < 100; i++) {
  const item = {};
  item.type = i + '';
  item.value = 10;
  if (i === 67) {
    item.value = 14;
  }
  if (i > 67) {
    item.value = 0;
  }
  data2.push(item);
}
export default function MonthlyEarning() {
    useEffect(() => {
        

    const chart = new Chart({
    container: 'c1',
    autoFit: true,
    height: 200,
    padding: 0,
    });
    chart.scale({
    type: {
        range: [0, 1],
    },
    value: {
        sync: true,
    },
    });
    chart.legend(false);
    chart.tooltip(false);

    const view1 = chart.createView();
    view1.data(data1);
    view1.axis(false);
    view1.coordinate('polar', {
    startAngle: (-9 / 8) * Math.PI,
    endAngle: (1 / 8) * Math.PI,
    innerRadius: 0.75,
    radius: 0.8,
    });
    view1
    .interval()
    .position('type*value')
    .color('#CBCBCB')
    .size(6);

    const view2 = chart.createView();
    view2.data(data1);
    view2.axis('value', false);
    view2.axis('type', {
    grid: null,
    line: null,
    tickLine: null,
    // label: {
    //     offset: -25,
    //     style: {
    //     textAlign: 'center',
    //     fill: '#CBCBCB',
    //     fontSize: 18,
    //     },
    //     formatter: (val) => {
    //     if (+val % 7 !== 0) {
    //         return '';
    //     }

    //     return val;
    //     },
    // },
    });
    view2.coordinate('polar', {
    startAngle: (-9 / 8) * Math.PI,
    endAngle: (1 / 8) * Math.PI,
    innerRadius: 0.95,
    radius: 0.55,
    });
    // view2
    //   .interval()
    //   .position('type*value')
    //   .color('#CBCBCB')
    //   .size(6);

    const view3 = chart.createView();
    view3.data(data2);
    view3.axis(false);

    view3.coordinate('polar', {
        startAngle: (-9 / 8) * Math.PI,
        endAngle: (1 / 8) * Math.PI,
        innerRadius: 0.75,
        radius: 0.8,
    });

    view3
    .interval()
    .position('type*value')
    .color('value', '#3023AE-#53A0FD')
    .size(6);

    view3.annotation().text({
    position: ['50%', '80%'],
    content: '67%',
    style: {
        fill: '#CBCBCB',
        fontSize: 20,
        textAlign: 'center',
        textBaseline: 'middle',
    },
    });

    view3.annotation().text({
    position: ['50%', '95%'],
    content: 'Series A',
    style: {
        fill: '#CBCBCB',
        fontSize: 16,
        textAlign: 'center',
        textBaseline: 'middle',
    },
    });
    chart.render();

    }, [])
    return (
        <div 
            className="rounded-sm p-4 bg-white"
            style={{
                boxShadow: "0 0.75rem 1.5rem rgba(18,38,63,.03)",
                // borderRadius:"2rem"
            }}
        >
            <div className="flex space-x-2 items-center">
                <div className="flex-1">
                    {/* gauge meter  */}
                    <div id="c1"></div>
                </div>
                <div className="flex-1 text-right ">
                    <p className="font-semibold text-sm ">
                        Monthly Earning
                    </p>
                    <p className="font-light text-sm py-4 color-light-grey opacity-75">
                        This month
                    </p>

                    <p className="font-semibold text-xl py-0">
                        $34,252
                    </p>

                    <p className="font-light text-sm  color-light-grey opacity-75 mt-1">
                        From 
                        <span style={{color:"#34c38f"}} className="font-semibold" >
                            <ArrowUpOutlined className="-mt-3 mx-1" style={{fontSize:"10px"}}/> 
                            12%
                        </span>
                        <br/>
                        previous period
                    </p>
                </div>
            </div>
            <div className="text-right">
                    <Button 
                        type="primary"
                        size="small"
                        style={{background:"#556ee6"}}    
                    >
                        <div className="flex items-center space-x-2">
                        <ArrowRightOutlined style={{fontSize:"8px"}}/> 
                        <span> View More</span>
                        </div>
                    </Button>
                    <p className="font-light text-sm  color-light-grey opacity-75">
                        We craft digital, graphic and dimensional thinking.
                    </p>
            </div>
        </div>
    )
}


