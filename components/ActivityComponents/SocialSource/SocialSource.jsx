import React from 'react'
import { FacebookFilled } from '@ant-design/icons';

export default function SocialSource() {
    return (
        <div 
            className="bg-white p-3"
            style={{boxShadow: "0 0.75rem 1.5rem rgba(18,38,63,.03)"}}
            >            
                <p className="font-semibold text-sm color-light-grey text-right">
                    Social Source
                </p>
                <div className="w-full flex items-center justify-between mt-10">
                    <FacebookFilled className="mx-auto h-10 w-10  text-2xl text-blue-700 p-3 bg-blue-100 rounded-full flex items-center justify-between" />
                </div>
                <p className="font-semibold text-base mt-4  text-center">
                    Facebook - <spam className="color-light-grey">125 sales</spam>
                </p>
                <p className="font-normal text-sm color-light-grey text-center mx-8">
                    Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus tincidunt.
                </p>
                <p className="font-normal text-sm text-blue-500 text-center mt-3">
                    {"> "} Learn more 
                </p>
                <div className="flex items-center justify-between mx-4 mt-8">
                    <div className="flex flex-col justify-center  text-center">
                        <img 
                            src="/svgs/insta.svg" 
                            className=" h-8" 
                            alt="insta"
                            />
                            <p className=" font-medium color-light-grey text-base mt-2 ">
                                Instagram
                            </p>
                            <p className="font-normal text-sm color-light-grey text-center">
                                sales 104
                            </p>
                    </div>
                    <div className="flex flex-col justify-center text-center">
                        <img 
                            src="/svgs/twitter.svg" 
                            className=" h-8" 
                            alt="twitter"
                            />
                            <p className=" font-medium color-light-grey text-base mt-2 ">
                                Twitter
                            </p>
                            <p className="font-normal text-sm color-light-grey text-center">
                                sales 112
                            </p>
                    </div>
                    <div className="flex flex-col justify-center  text-center">
                        <img 
                            src="/svgs/fb.svg" 
                            className=" h-8" 
                            alt="fb"
                            />
                            <p className=" font-medium color-light-grey text-base mt-2 ">
                                Facebook
                            </p>
                            <p className="font-normal text-sm color-light-grey text-center">
                                sales 125
                            </p>
                    </div>
                </div>
        </div>
    )
}
