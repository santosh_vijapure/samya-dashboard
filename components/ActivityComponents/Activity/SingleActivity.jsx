import React from 'react'
import { ArrowRightOutlined } from '@ant-design/icons';

export default function SingleActivity({activity,date}) {
    return (
                    <div className="flex items-start justify-between">
                        <p className="font-normal text-sm color-light-grey text-right">
                            {activity}
                        </p>
                        <ArrowRightOutlined className="text-blue-700 ml-2 mt-1" style={{fontSize:"13px"}}/> 
                        <p className="mx-4 mt-1  font-bold text-xs color-light-grey text-center no-word-wrap">
                            {date}
                        </p>
                        <img src="/svgs/right-arrow-circle.svg" alt="arrow" className=" bg-white h-5 w-5 -mr-3  mt-1"/>
                    </div>
    )
}
