import React from 'react'
import { ArrowRightOutlined } from '@ant-design/icons';
import SingleActivity from './SingleActivity';
import { Button } from 'antd';

export default function Activity() {
    return (
<div 
            className="bg-white p-3 "
            style={{boxShadow: "0 0.75rem 1.5rem rgba(18,38,63,.03)"}}
            >            
                <p className="font-semibold text-sm color-light-grey text-right">
                    Activity
                </p>
                <div className="activity-line mt-4 space-y-6 pt-4">

                    {/* <div className="flex items-start justify-between">
                        <p className="font-normal text-sm color-light-grey text-right">
                            Responded to need “Volunteer Activities
                        </p>
                        <ArrowRightOutlined className="text-blue-700" style={{fontSize:"13px"}}/> 
                        <p className="mx-4  font-bold text-xs color-light-grey text-center no-word-wrap">
                            NOV 12
                        </p>
                        <img src="/svgs/right-arrow-circle.svg" alt="arrow" className=" bg-white h-5 w-5 -mr-3"/>
                    </div>
                    
                    <div className="flex items-start justify-between ">
                        <p className="font-normal text-sm color-light-grey text-right">
                        Everyone realizes why a new common language would be desirable..
                        </p>
                        <ArrowRightOutlined className="text-blue-700" style={{fontSize:"13px"}}/> 
                        <p className="mx-4 font-bold text-xs color-light-grey text-center no-word-wrap">
                            NOV 12
                        </p>
                        <img src="/svgs/right-arrow-circle.svg" alt="arrow" className=" bg-white h-5 -mr-3"/>
                    </div> */}

                    <SingleActivity
                        activity={"Responded to need “Volunteer Activities"}
                        date ="12 Nov"
                    />

                    <SingleActivity
                        activity={"Everyone realizes why a new common language would be desirable"}
                        date ="17 Nov"
                    />

                    <SingleActivity
                        activity={"Joined the group “Boardsmanship Forum”"}
                        date ="15 Nov"
                    />
                    <SingleActivity
                        activity={"Responded to need “In-Kind Opportunity”"}
                        date ="12 Nov"
                    />

                </div>
                <div
                        className="flex items-center justify-center mt-5"    
                >

                    <Button
                        type="primary"
                        size="small"
                        style={{background:"#556ee6"}}
                    >
                        <div className="flex items-center space-x-2">
                        <ArrowRightOutlined style={{fontSize:"8px"}}/> 
                        <span> View More</span>
                        </div>
                    </Button>
                </div>
        </div>
    )
}
