import React from 'react'

export default function TopCities() {
    return (
        <div 
            className="bg-white p-3 color-light-grey"
            style={{boxShadow: "0 0.75rem 1.5rem rgba(18,38,63,.03)"}}
            >
                <p className="font-semibold text-sm  text-right">
                    Top Cities Selling Product
                </p>
                <img 
                    src="/imgs/pin.png" 
                    className="mx-auto h-20 mt-6"
                    alt="pin"/>
                <p className="font-semibold text-xl  text-center mt-4">
                    1,456
                </p>
                <p className="font-normal text-sm  text-center mt-2">
                    San Francisco
                </p>
                <hr className="mt-6"/>
                <div className="flex items-center justify-between py-2">
                    <progress className="" value="80" max="100"/>
                    <p className="font-normal text-base  text-right w-full mt-2">
                        1,456
                    </p>
                    <p className="font-normal text-sm  text-right w-full mt-2">
                        San Francisco
                    </p>
                </div>
                <hr className=""/>

                <div className="flex items-center justify-between py-2">
                    <progress className="" value="60" max="100"/>
                    <p className="font-normal text-base  text-right w-full mt-2">
                        1,123
                    </p>
                    <p className="font-normal text-sm  text-right w-full mt-2">
                        Los Angeles
                    </p>
                </div>
                
                <hr className=""/>

                <div className="flex items-center justify-between py-2">
                    <progress className="" value="40" max="100"/>
                    <p className="font-normal text-base  text-right w-full mt-2">
                        1,026
                    </p>
                    <p className="font-normal text-sm  text-right w-full mt-2">
                        San Diego
                    </p>
                </div>

        </div>
    )
}
