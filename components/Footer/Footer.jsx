import React from 'react';

const Footer = () => {
    return (
        <div
            className="flex items-center justify-between px-20 py-4 mt-10" 
            style={{background:"#F2F2F5"}}
            >

            <p className="font-light text-sm py-4 color-light-grey opacity-75">
                Design & Develop by x-dev
            </p>

            <p className="font-light text-sm py-4 color-light-grey opacity-75">
                2021 © Skote.
            </p>

        </div>
    );
}

export default Footer;
