import Text from 'antd/lib/typography/Text'
import React from 'react'

export default function BreadcrumbSection() {
    return (
        <div className="py-3 sm:px-20 flex items-center justify-between color-light-grey">
            <div className="flex items-center text-sm font-normal space-x-2 ">
                <Text className="cursor-pointer">
                    Dashboard
                </Text> 
                <span>
                   {"/"} 
                </span>
                <Text className="cursor-pointer">
                    Dashboards
                </Text>
            </div>
            <Text className="text-base font-semibold color-light-grey">
            DASHBOARD
            </Text>
        </div>
    )
}
