import React, { useEffect } from 'react'


import { Chart } from '@antv/g2';

const data = [
  { time: 'JAN', type: 'Series C', value: 52000 },
  { time: 'JAN', type: 'Series B', value: 40000 },
  { time: 'JAN', type: 'Series A', value: 37000 },
  { time: 'FEB', type: 'Series C', value: 35000 },
  { time: 'FEB', type: 'Series B', value: 32000 },
  { time: 'FEB', type: 'Series A', value: 20000 },
  { time: 'MAR', type: 'Series C', value: 59000 },
  { time: 'MAR', type: 'Series B', value: 47000 },
  { time: 'MAR', type: 'Series A', value: 34000 },
  { time: 'APR', type: 'Series C', value: 44000 },
  { time: 'APR', type: 'Series B', value: 42000 },
  { time: 'APR', type: 'Series A', value: 38000 },
  { time: 'MAY', type: 'Series C', value: 47000 },
  { time: 'MAY', type: 'Series B', value: 44000 },
  { time: 'MAY', type: 'Series A', value: 50000 },
  { time: 'JUN', type: 'Series C', value: 48000 },
  { time: 'JUN', type: 'Series B', value: 40000 },
  { time: 'JUN', type: 'Series A', value: 42000 },
  { time: 'JUL', type: 'Series C', value: 50000 },
  { time: 'JUL', type: 'Series B', value: 48000 },
  { time: 'JUL', type: 'Series A', value: 44000 },
  { time: 'AUG', type: 'Series C', value: 52000 },
  { time: 'AUG', type: 'Series B', value: 30000 },
  { time: 'AUG', type: 'Series A', value: 48000 },
  { time: 'SEP', type: 'Series C', value: 54000 },
  { time: 'SEP', type: 'Series B', value: 22000 },
  { time: 'SEP', type: 'Series A', value: 40000 },  
  { time: 'OCT', type: 'Series C', value: 56000 },
  { time: 'OCT', type: 'Series B', value: 54000 },
  { time: 'OCT', type: 'Series A', value: 52000 },
  { time: 'NOV', type: 'Series C', value: 58000 },
  { time: 'NOV', type: 'Series B', value: 36000 },
  { time: 'NOV', type: 'Series A', value: 44000 },
  { time: 'DEC', type: 'Series C', value: 60000 },
  { time: 'DEC', type: 'Series B', value: 48000 },
  { time: 'DEC', type: 'Series A', value: 26000 },
];


export default function LineChart() {
    useEffect(() => {
        const chart = new Chart({
        container: 'chart',
        autoFit: true,
        height: 500,
        width:500
        });
        chart.data(data);
        // chart.scale('value', {
        // alias: ''
        // });
        chart.axis('time', {
        tickLine: null,
        });

        chart.axis('value', {
        label: {
            formatter: text => {
            return text.replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');
            }
        },
        title: {
            offset: 80,
            style: {
            fill: '#aaaaaa'
            },
        }
        });
        chart.legend({
        position: 'bottom',
        });

        chart.tooltip({
        shared: true,
        showMarkers: false,
        });
        chart.interaction('active-region');

        chart
        .interval()
        .adjust('stack')
        .position('time*value')
        .color('type', ['#72E8B7', '#F1B44C', '#556EE6']);
        chart.render();

    }, [])
    return (
        <div 
            className="p-10 max-h-full bg-white"
            style={{
                boxShadow: "0 0.75rem 1.5rem rgba(18,38,63,.03)",
                // borderRadius:"2rem"
            }}>

            <div id="chart"></div>
        </div>
    )
}
