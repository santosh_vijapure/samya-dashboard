import React from 'react'
import Text from 'antd/lib/typography/Text';

export default function PriceCard({icon,title,priceText}) {
    return (
        <div 
            className="p-4 flex justify-between items-center rounded-sm  bg-white w-full sm:w-60 h-24"
            style={{boxShadow: "0 0.75rem 1.5rem rgba(18,38,63,.03)"}}>
            <div 
                className="p-2 px-3 rounded-full flex justify-center items-center h-12 w-12 text-white" 
                style={{background:"#556ee6"}}
            >
                {icon}
            </div>
            <div className="text-right">
                <Text className="text-sm font-normal color-light-grey ">
                   {title} 
                </Text>
                <br/>
                <Text className="text-xl font-normal color-light-grey">
                   {priceText} 
                </Text>
            </div>
        </div>
    )
}
