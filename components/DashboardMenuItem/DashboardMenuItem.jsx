import React from 'react'

export default function DashboardMenuItem({title,icon}) {
    return (
        <div className="flex items-center space-x-1">
            <img src="/svgs/down-arrow-white.svg" alt="down-arrow" className="h-4 transform -rotate-90 "></img>
            <span className="text-sm font-normal text-white ">{title}</span>
            {icon}
        </div>
    )
}
