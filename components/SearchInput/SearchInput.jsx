import React from "react";
import {SearchOutlined} from '@ant-design/icons';


export default function SearchInput({
  handleSearch,
  placeholder,
  name,
}) {
  return (
    <div className="relative h-full w-full rounded-full" >
      <input
        type="text"
        name={name}
        placeholder={placeholder}
        onChange={handleSearch}
        className="search-input rounded w-full p-3 pl-8 text-xs outline-none rounded-full"
        style={{background:"#F3F3F9"}}
      />
      <div 
        className="absolute left-0 top-0 p-2 pt-1"
      >
        <SearchOutlined />
      </div>

    </div>
  );
}
