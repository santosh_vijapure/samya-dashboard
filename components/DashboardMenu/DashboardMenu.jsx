import React from 'react'
import DashboardMenuItem from '../DashboardMenuItem/DashboardMenuItem'
import { AppstoreAddOutlined,HomeOutlined ,FileOutlined,CodepenCircleOutlined,MenuFoldOutlined} from '@ant-design/icons';
import {Menu, Dropdown} from 'antd';

const menu = (
  <Menu>
    <Menu.Item>
        1st menu item 
    </Menu.Item>
    <Menu.Item>
        2nd menu item
    </Menu.Item>
    <Menu.Item>
        3rd menu item
    </Menu.Item>
  </Menu>
);
export default function DashboardMenu() {
    return (
        <div
        className="py-4 sm:px-20 flex space-x-10 items-center justify-end" 
        style={{backgroundColor:"#556EE5"}}
        >
            <Dropdown overlay={menu} className="cursor-pointer">
                <div>
                    <DashboardMenuItem 
                        title="Extra pages" 
                        icon={<FileOutlined style={{color:"#fff",fontSize:"13px"}} />}
                    />
                </div>
            </Dropdown>
            <Dropdown overlay={menu} className="cursor-pointer">
                <div>
                    <DashboardMenuItem 
                        title="Components" 
                        icon={<MenuFoldOutlined style={{color:"#fff",fontSize:"13px"}} />}
                    />
                </div>
            </Dropdown>
            <Dropdown overlay={menu} className="cursor-pointer">
                <div>
                    <DashboardMenuItem 
                        title="Apps" 
                        icon={<AppstoreAddOutlined style={{color:"#fff",fontSize:"13px"}} />}
                    />
                </div>
            </Dropdown>
            <Dropdown overlay={menu} className="cursor-pointer">
                <div>
                    <DashboardMenuItem 
                        title="UI Elements" 
                        icon={<CodepenCircleOutlined style={{color:"#fff",fontSize:"13px"}} />}
                    />
                </div>
            </Dropdown>
            <Dropdown overlay={menu} className="cursor-pointer">
                <div>
                    <DashboardMenuItem 
                        title="Dashboard" 
                        icon={<HomeOutlined style={{color:"#fff",fontSize:"13px"}} />}
                    />
                </div>
            </Dropdown>

        </div>
    )
}
