import React from 'react'
import { Spin ,Menu, Dropdown, Input} from 'antd';
import { SettingOutlined ,BellOutlined,AppstoreAddOutlined ,FullscreenOutlined} from '@ant-design/icons';
import Avatar from 'antd/lib/avatar/avatar';
import SearchInput from "../SearchInput";

const menu = (
  <Menu>
    <Menu.Item>
        1st menu item
    </Menu.Item>
    <Menu.Item>
        2nd menu item
    </Menu.Item>
    <Menu.Item>
        3rd menu item
    </Menu.Item>
    <Menu.Item>
        4rd menu item
    </Menu.Item>
  </Menu>
);

export default function PageHeaderLayout() {
    const antIcon = <SettingOutlined style={{ fontSize: 20,color:'#555b6d' }} spin />;
    return (
        <div className="p-4 flex justify-between sm:px-20 items-center bg-white"  style={{color:'#555b6d'}}>  
        {/* left section */}
             <div className="flex items-center space-x-6 ">
                <Spin className="cursor-pointer" indicator={antIcon} ></Spin>
                <Dropdown overlay={menu} className="flex items-center space-x-1 cursor-pointer">
                    <span className="text-sm  font-norma" >
                    <img src="/svgs/down-arrow-black.svg" alt="down-arrow" className="h-3 mr-1  transform -rotate-90"></img>
                    admin <Avatar src="/imgs/avatar.jpg"/>
                    </span>
                </Dropdown>

                <BellOutlined className="cursor-pointer"  style={{ fontSize: 20 }}/>
                <FullscreenOutlined className="cursor-pointer" style={{ fontSize: 20 }} />
                <AppstoreAddOutlined  className="cursor-pointer" style={{ fontSize: 20 }}/>
                <Dropdown className="cursor-pointer" overlay={menu}>
                    <span className="text-sm  font-normal  flex items-center space-x-1" >
                        <span>English</span>
                        <img className='h-4 w-6' src="https://upload.wikimedia.org/wikipedia/commons/a/a4/Flag_of_the_United_States.svg" alt="us-flag"/>
                    </span>
                </Dropdown>
             </div>

        {/* right section */}
             <div className="flex items-center space-x-6 ">
                <Dropdown className="cursor-pointer" overlay={menu}>
                    <span className="text-sm  font-normal  flex items-center space-x-1 w-20 no-word-wrap" >
                   Main Menu
                    </span>
                </Dropdown>
                {/* <Input placeholder="Search..." prefix={<SearchOutlined />} className="rounded-full" style={{borderRadius:"50%"}}/> */}
                <SearchInput 
                    placeholder="Search..."
                />
                <img  src="/imgs/logo.png" className="h-6 cursor-pointer"></img>
             </div>

        </div>
    )
}
