import { Button } from 'antd'
import Avatar from 'antd/lib/avatar/avatar'
import Text from 'antd/lib/typography/Text'
import React from 'react'
import { ArrowRightOutlined } from '@ant-design/icons';

export default function ProfileCard() {
    return (
        <div 
            className="rounded-sm  bg-white"
            style={{
                boxShadow: "0 0.75rem 1.5rem rgba(18,38,63,.03)",
                // borderRadius:"2rem"
            }}>
            <div
                className="  flex items-center justify-between rounded-t-sm pr-2"
                style={{background:"#D4DBF9"}}
            >
                <img 
                src="/imgs/profile-desk.png"
                className="h-20" 
                />
                <p
                className="text-sm p-3 pb-4"
                style={{color:"#556ee6"}}
                >
                    Welcome Back !
                    <br/>
                    <spam className="font-normal"> Skote Dashboard</spam>
                </p>
            </div>
            <div className="flex  justify-around relative ">
                <div className="flex flex-col pt-4 text-right">
                    <p className="font-medium text-lg ">
                        $1245
                    </p>
                     <p className=" text-sm  font-light">
                        Revenue                   
                    </p>
                </div>
                
                <div className="flex flex-col pt-4 text-right">
                    <p className="font-medium text-lg ">
                        125
                    </p>
                
                    <p className=" text-sm  font-light">
                        Projects                   
                    </p>
                </div>
                <div></div>
                <img  
                    src="/imgs/avatar.jpg" 
                    className="h-16 rounded-full absolute right-4 p-1 bg-white -mt-6"
                />
            </div>
            <div className="flex  justify-end relative space-x-4 items-end pb-6 pr-4">
                <div>
                    <Button 
                        type="primary"  
                        size="small"
                        style={{background:"#556ee6"}}    
                    >
                    <div className="flex items-center space-x-2">
                       <ArrowRightOutlined style={{fontSize:"8px"}}/> 
                       <span> View Profile</span>
                    </div>
                    </Button>
                </div>
                <div className="flex flex-col  text-right">
                    <p className="font-medium text-lg ">
                        Henry Price
                    </p>
                    <p className=" text-sm  font-light">
                        UI/UX Designer                   
                    </p>
                </div>
            </div>
        </div>
    )
}
