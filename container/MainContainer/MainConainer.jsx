import Modal from 'antd/lib/modal/Modal'
import React, { useState } from 'react'
import BreadcrumbSection from '../../components/BreadcrumbSection'
import DashboardMenu from '../../components/DashboardMenu'
import Footer from '../../components/Footer/Footer'
import PageHeaderLayout from '../../components/PageHeader'
import ActivityContainer from '../ActivityContainer'
import ChartAndProfileContainer from '../ChartAndProfileContainer'
import TableContainer from '../TableContainer'

export default function MainConainer() {
      const [visible, setVisible] = useState(false);

    return (
        <div className="relative text-xl font-semibold mt-32" style={{background:"#F8F8FB"}}>
            <div className="fixed top-0 w-screen z-40">
                <PageHeaderLayout/>
                <DashboardMenu/>
            </div>
            <BreadcrumbSection/>
            <ChartAndProfileContainer/>
            <ActivityContainer/>
            <TableContainer setModalVisiblity={setVisible}/>
            <Footer/>
            <Modal
            title="Order Details"
            centered
            visible={visible}
            onOk={() => setVisible(false)}
            onCancel={() => setVisible(false)}
            width={"30%"}
          >
            <p>some contents...</p>
            <p>some contents...</p>
            <p>some contents...</p>
          </Modal>
        </div>
    )
}
