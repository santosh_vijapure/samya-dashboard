import React from 'react';
import PriceCard from '../../components/PriceCard/PriceCard';
import { CopyOutlined,TagOutlined ,InboxOutlined} from '@ant-design/icons';
import ProfileCard from '../../components/ProfileCard';
import MonthlyEarning from '../../components/MonthlyEarning';
import LineChart from '../../components/LineChart/LineChart';

export default function ChartAndProfileContainer() {
    return (
        <div className="flex sm:px-20 space-x-6 " >
            <div className="flex flex-col space-y-6 ">
                {/* pricing and chart column */}
                <div className="sm:flex sm:space-x-10 ">
                    {/* pricing cards row */}
                    <PriceCard
                        title="Average Price"
                        priceText="$16.2"
                        icon={<TagOutlined/>}
                    />
                    <PriceCard
                        title="Revenue"
                        priceText="$35, 723"
                        icon={<InboxOutlined/>}

                    />
                    <PriceCard
                        title="Orders"
                        priceText="1,235"
                        icon={<CopyOutlined/>}

                    />
                </div>
                <div>
                    {/* charting */}
                    <LineChart/>
                </div>
            </div>

            <div className="space-y-6" style={{width:"35%"}}>
                {/* profile and monthly earing column */}
                <div>
                    {/* profile card */}
                    <ProfileCard/>
                </div>
                <div>
                    {/* monthly earing card */}
                    <MonthlyEarning/>
                </div>

            </div>
        </div>
    )
}
