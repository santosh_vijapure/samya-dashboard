import React from 'react'
import Activity from '../../components/ActivityComponents/Activity'
import  TopCities from "../../components/ActivityComponents/TopCities";
import  SocialSource from "../../components/ActivityComponents/SocialSource";
export default function ActivityContainer() {
    return (
        <div className="sm:mx-20 flex items-start space-x-6  mt-6">
            <div className="flex-1">
                <TopCities/>
            </div>

            <div className="flex-1">
                <Activity/>
            </div>

            <div className="flex-1">
                <SocialSource/>
            </div>
        </div>
    )
}
