import React, { useState } from 'react'
import { Button, Table, Tag } from 'antd';
import {ApolloClient, InMemoryCache, gql, useQuery} from '@apollo/client';
// initialize a GraphQL client
const client = new ApolloClient({
  cache: new InMemoryCache(),
  uri: 'https://countries.trevorblades.com'
});

//  GraphQL query 
const LIST_COUNTRIES = gql`
  {
    countries {
        name
        code
        phone
        capital
        native
        currency
    }
  }
`;



function onChange(pagination, filters, sorter, extra) {
  console.log('params', pagination, filters, sorter, extra);
}

export default function TableContainer({setModalVisiblity}) {
    const [selectedRowKeys, setSelectedRowKeys] = useState([])
    const {data, loading, error} = useQuery(LIST_COUNTRIES, {client});
    // rowSelection object indicates the need for row selection
    const onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        setSelectedRowKeys({ selectedRowKeys });
    };
    const rowSelection = {
      selectedRowKeys,
      onChange: onSelectChange,
    };


    if (loading || error) {
        return <p className="sm:mx-20 h-80 flex items-center justify-center">{error ? error.message : 'Loading...'}</p>;
    }
    console.log(data);


    const columns=[
        {
        title: "Name",
        dataIndex: "name", 
                sorter: (a, b) => a.name.localeCompare(b.name),
        },
        {
            title:"Code",
            dataIndex:"code",
            width:"3rem"

        },
        {
            title:"Phone",
            dataIndex:"phone",
            sorter:{
                        compare: (a, b) => a.phone - b.phone,
            }
        },
        {
            title:"Capital",
            dataIndex:"capital"
        },
        {
        title:"Currency",
        dataIndex:"currency",
        render:currency=> currency && currency.split(",").map(cur=>(
            <Tag color={"green"}>{cur}</Tag>
        )),
        width:"5rem"
        },{
            title:"Details",
            dataindex:null,
            width:"8rem",
            render: _ => (
                <Button 
                    style={{background:"#556ee6",color:'white'}}
                    className="rounded-full text-sm"
                    onClick={()=>setModalVisiblity(true)}
                    >
                    More Details 
                </Button>
            ),

        }
    ]

    return (
        <div
            className="sm:mx-20 mt-10 bg-white"
            style={{boxShadow: "0 0.75rem 1.5rem rgba(18,38,63,.03)"}}>
                <Table 
                    columns={columns}
                    dataSource={data.countries}
                    onChange={onChange}
                    bordered  
                    // rowSelection={rowSelection}
                    />  
        </div>
    )
}
